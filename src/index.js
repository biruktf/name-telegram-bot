import { actionHandler } from './handlers/actions'

const mongoose = require('mongoose'),
    Telegraf = require('telegraf'),
    Stage = require('telegraf/stage'),
    session = require('telegraf/session')

import { config } from './config/config'

const express = require('express')
const expressApp = express()
expressApp.set('view engine', 'ejs')

export const { leave, enter } = Stage

import { startHandler } from './handlers/commands/startHandler'
import { newNameHandler } from './handlers/commands/newNameHandler'
import { editNameHandler } from './handlers/commands/editNameHandler'
import { listHandler } from './handlers/commands/listHandler'
import { helpHandler } from './handlers/commands/helpHandler'
import { cancelHandler } from './handlers/commands/cancelHandler'
import { adminHandler } from './handlers/commands/adminHandler'
import { feedbackHandler } from './handlers/commands/feedbackHandler'

import { descriptionScene } from './handlers/scenes/newNameScenes/descriptionScene'
import { genderScene } from './handlers/scenes/newNameScenes/genderScence'
import { nameScene } from './handlers/scenes/newNameScenes/nameScene'
import { originScene } from './handlers/scenes/newNameScenes/originScene'
import { celebrityScene } from './handlers/scenes/newNameScenes/celebrityScence'
import { publishScene } from './handlers/scenes/publishScene'
import { isCommand } from './handlers/checkCommand'
import { firstScene } from './handlers/scenes/editNameScenes/firstScene'
import { editGenderScene } from './handlers/scenes/editNameScenes/editGenderScene'
import { editOriginScene } from './handlers/scenes/editNameScenes/editOriginScene'
import { editDescriptionScene } from './handlers/scenes/editNameScenes/editDescriptionScene'
import { editCelebrityScene } from './handlers/scenes/editNameScenes/editCelebrityScene'
import { editOtherVersionScene } from './handlers/scenes/editNameScenes/editOtherVersionScene'
import { editShortFormScene } from './handlers/scenes/editNameScenes/editShortFormScene'
import { otherVersionScene } from './handlers/scenes/newNameScenes/otherVersionScene'
import { shortFormScene } from './handlers/scenes/newNameScenes/shortFormScene'
import { feedbackScene } from './handlers/scenes/feedbackScene'
import { exampleHandler } from './handlers/commands/exampleHandler'
import { NameModel } from './models/name-model'

//connect to the database
mongoose
    .connect(config.dbURI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log('database Connection Established!')
    })
    .catch(e => console.log('database Connection failed\n', e))
//create telegram instance

export const Bot = new Telegraf(config.TOKEN, {
    telegram: {
        webhookReply: true,
    },
})
// if (process.env.NODE_ENV === 'production') {
//     expressApp.use(Bot.webhookCallback('/' + config.TOKEN))
//     Bot.telegram.setWebhook(config.URL + '/' + config.TOKEN)
//     // Bot.launch()
// } else {
//     Bot.telegram.deleteWebhook().then(() => {
//         Bot.startPolling()
//         console.log('polling started')
//     })
// }

expressApp.use(Bot.webhookCallback('/' + config.TOKEN))
Bot.telegram.setWebhook(config.URL + '/' + config.TOKEN)

expressApp.get('/', (req, res) => {
    res.send('Hello  Name Archive Bot Agina!')
})

expressApp.listen(config.PORT, () => {
    console.log(`Listening on port ${config.PORT}`)
})

Bot.use(
    session({
        getSessionKey: ctx => {
            if (ctx.from && ctx.chat) {
                return `${ctx.from.id}:${ctx.chat.id}`
            } else if (ctx.from && ctx.inlineQuery) {
                return `${ctx.from.id}:${ctx.from.id}`
            }
            return null
        },
    })
)

//handle actions
actionHandler(Bot)
isCommand(Bot)

//create Stage and Register scenes
export const stage = new Stage([
    nameScene,
    originScene,
    genderScene,
    descriptionScene,
    celebrityScene,
    publishScene,
    otherVersionScene,
    shortFormScene,
    firstScene,
    editGenderScene,
    editOriginScene,
    editDescriptionScene,
    editCelebrityScene,
    editOtherVersionScene,
    editShortFormScene,
    feedbackScene,
])
Bot.use(stage.middleware()).catch(e =>
    console.log('Error while switching scenes', e)
)

// attach some commands
Bot.start(startHandler)
Bot.help(helpHandler)
Bot.command('/post', newNameHandler)
Bot.command('/editname', editNameHandler)
Bot.command('/mynames', listHandler)
Bot.command('/cancel', cancelHandler)
Bot.command('/example', exampleHandler)
Bot.command('/feedback', feedbackHandler)
Bot.command('/BBBBBT6', adminHandler)
