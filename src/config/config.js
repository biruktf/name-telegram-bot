const dotenv = require('dotenv').config()

export const config = {
    dbURI: process.env.DB_HOST,
    TOKEN: process.env.TOKEN,
    admin_chat_id: process.env.ADMIN_ID,
    channel_chat_username: process.env.CHANNEL_NAME,
    redisUrl: 'redis://localhost:6379',
    URL: process.env.URL,
    PORT: process.env.PORT,
}
