export const displayFull = (Data, isAdmin = false) => {
    let otherV = Data.otherVersions
        ? Data.otherVersions.toString().split(',')
        : ''
    let shortF = Data.shortForm ? Data.shortForm.toString().split(',') : ''
    let created_by = Data.createdBy ? Data.createdBy.toString().split(',') : ''
    let versions = ''
    let form = ''
    let by = ''
    for (let i of created_by) {
        by = by + `@${i} `
    }
    for (let i of otherV) {
        versions = versions + ` #${i}`
    }
    for (let i of shortF) {
        form = form + ` #${i}`
    }
    return `*Name*: ${changeToCapital(Data.name)}\n*Gender*: ${Data.gender}\n*Origin*: ${
        Data.origin
    }\n*Description*:${Data.description}\n*Short Form*: ${
        Data.shortForm
    }\n*Other Versions*: ${Data.otherVersions}\n*Celebrities*:  ${
        Data.celebrity
    } \n\n#${Data.name}${versions}${form}\n${isAdmin ? by : ''} \n@namearchive`


}

export const editedVersion = async ctx => {
    let EditData = ctx.session.editData
    let Data = await reAssign(ctx.session.data)

    if (Data !== null || EditData !== null) {
        EditData.name = Data.name
        EditData.previous_name_id = Data.name_id
        EditData.gender = EditData.gender ? EditData.gender : Data.gender
        if (EditData.origin !== null && EditData.origin !== undefined) {
            EditData.origin = Data.origin + ',' + EditData.origin
        } else {
            EditData.origin = Data.origin
        }

        if (EditData.description !== null && EditData.description !== undefined) {
            EditData.description =
                Data.description + '\n' + EditData.description
        } else {
            EditData.description = Data.description
        }
        if (EditData.shortForm !== null && EditData.shortForm !== undefined) {
            EditData.shortForm = Data.shortForm + ',' + EditData.shortForm
        } else {
            EditData.shortForm = Data.shortForm
        }
        if (EditData.otherVersions !== null && EditData.otherVersions !== undefined) {
            EditData.otherVersions =
                Data.otherVersions + ',' + EditData.otherVersions
        } else {
            EditData.otherVersions = Data.otherVersions
        }

        if (EditData.celebrity !== null && EditData.celebrity !== undefined) {
            EditData.celebrity = Data.celebrity + ',' + EditData.celebrity
        } else {
            EditData.celebrity = Data.celebrity
        }
    }

    ctx.session.editData = EditData
    ctx.session.data = Data
}

export const initializeSession = ctx => {
    if (!ctx.session) ctx.session = { data: {}, editData: {} }
    if (!ctx.session.data) ctx.session.data = {}
    if (!ctx.session.editData) ctx.session.editData = {}
}
export const resetSession=(ctx)=>{
    ctx.session = { data: {}, editData: {} }
}


async function reAssign (doc) {
    return {
        name: doc.name?doc.name:'',
        gender: doc.gender?doc.gender:'',
        origin: doc.origin?doc.origin:'',
        description: doc.description?doc.description:'',
        celebrity: doc.celebrity?doc.celebrity:'',
        shortForm:doc.shortForm?doc.shortForm:'',
        otherVersions:doc.otherVersions?doc.otherVersions:'',
        name_id:doc.name_id
    }
}

function changeToCapital(str){return str.charAt(0).toUpperCase()+str.slice(1)}

