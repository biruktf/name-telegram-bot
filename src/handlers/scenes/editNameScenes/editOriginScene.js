import Scene from 'telegraf/scenes/base'
import { initializeSession } from '../../utils'

export const editOriginScene = new Scene('edit-origin') // pass the name of the scene here
editOriginScene.enter(async (ctx) => {
    initializeSession(ctx)
    await ctx.reply('ተጨማሪ ምንጭ|Add Origin:')
})
editOriginScene.on('message', ctx => {
    ctx.session.editData.origin = ctx.message.text.toString()
})
editOriginScene.leave()
