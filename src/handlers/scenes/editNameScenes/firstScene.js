import Scene from 'telegraf/scenes/base'
import { NameModel } from '../../../models/name-model'
import { Markup } from 'telegraf'
import { displayFull } from '../../utils'


export const firstScene = new Scene('firstScene')
firstScene.enter(async (ctx) => {
   await ctx.reply(
        'If the name is already added, you can add additional information'
    )
   await ctx.reply('ስም|Enter the existing *Name*:',{
        parse_mode:'Markdown'
    })
})
firstScene.on('message', async (ctx) => {
    let searchName =ctx.message.text.toString().toLowerCase();

    await NameModel.findOne({name: searchName})
        .then(async (data)=>{
            if(data !== null) {
                ctx.session.data.name_id = data._id
                ctx.session.data.name = data.name
                ctx.session.data.origin = data.origin
                ctx.session.data.gender = data.gender
                ctx.session.data.shortForm = data.shortForm
                ctx.session.data.otherVersions = data.otherVersions
                ctx.session.data.description = data.description
                ctx.session.data.celebrity = data.celebrity
                ctx.session.data.createdBy = data.createdBy
                ctx.reply(displayFull(ctx.session.data),{
                    parse_mode:'Markdown'
                })
               await ctx.reply(
                    'Choose the field you want edit(add) information.\nwhen you are done click *save changes* button',
                    {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            'inline_keyboard': [
                                [
                                    Markup.callbackButton(
                                        'Add name version',
                                        'EDIT_OTHER_VERSION'
                                    ),
                                ],
                                [
                                    Markup.callbackButton(
                                        'Add short form ',
                                        'EDIT_SHORT_FORM'
                                    ),
                                ],
                                [
                                    Markup.callbackButton('Gender', 'EDIT_GENDER'),
                                    Markup.callbackButton('Origin', 'EDIT_ORIGIN'),
                                ],
                                [
                                    Markup.callbackButton(
                                        'Description',
                                        'EDIT_DESCRIPTION'
                                    ),
                                    Markup.callbackButton(
                                        'Celebrities',
                                        'EDIT_CELEBRITIES'
                                    ),
                                ],
                                [Markup.callbackButton('Save changes', 'SAVE_CHANGES')],
                            ]
                        })
                    }
                )
            }else {
              await ctx.reply("can't find the name 😕 ")
            }
        }).catch(async (e)=>{
            console.log(e)
            await ctx.reply("Can't find the name 😕")
        })

})
firstScene.action('EDIT_OTHER_VERSION', async (ctx) => {
   await ctx.answerCbQuery('other-version')
    ctx.scene.enter('edit-other-version')
})
firstScene.action('EDIT_SHORT_FORM', async (ctx) => {
   await ctx.answerCbQuery('short-from')
    ctx.scene.enter('edit-short-form')
})
firstScene.action('EDIT_GENDER', async (ctx) => {
   await ctx.answerCbQuery('gender')
    ctx.scene.enter('edit-gender')
})
firstScene.action('EDIT_ORIGIN', async (ctx) => {
   await ctx.answerCbQuery('origin')
    ctx.scene.enter('edit-origin')
})
firstScene.action('EDIT_DESCRIPTION', async (ctx) => {
   await ctx.answerCbQuery('description')
    ctx.scene.enter('edit-description')
})
firstScene.action('EDIT_CELEBRITIES', async (ctx) => {
   await ctx.answerCbQuery('celebrity')
    ctx.scene.enter('edit-celebrity')
})


firstScene.leave()
