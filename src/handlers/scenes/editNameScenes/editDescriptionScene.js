import Scene from 'telegraf/scenes/base'
import { initializeSession } from '../../utils'

export const editDescriptionScene = new Scene('edit-description') // pass the name of the scene here
editDescriptionScene.enter(async (ctx) => {
    initializeSession(ctx)
   await ctx.reply('ተጨማሪ ትርጉም ጨምር|Additional description')
})
editDescriptionScene.on('message', ctx => {
    ctx.session.editData.description = ctx.message.text.toString()
})
editDescriptionScene.leave()
