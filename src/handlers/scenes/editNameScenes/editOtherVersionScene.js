import Scene from 'telegraf/scenes/base'
import { EditData } from '../../commands/editNameHandler'
import { initializeSession } from '../../utils'

export const editOtherVersionScene = new Scene('edit-other-version') // pass the name of the scene here
editOtherVersionScene.enter(async (ctx) => {
    initializeSession(ctx)
    await ctx.reply('ተጨማሪ የስሙ አፃፃፍ |Add Other versions,_comma separated_',{
        parse_mode:'Markdown'
    })
})
editOtherVersionScene.on('message', ctx => {
    ctx.session.editData.otherVersions = ctx.message.text.toString()
})

editOtherVersionScene.leave()
