import Scene from 'telegraf/scenes/base'
import { initializeSession } from '../../utils'

export const editGenderScene = new Scene('edit-gender') // pass the name of the scene here
editGenderScene.enter(async (ctx) => {
    initializeSession(ctx)
    await ctx.reply('ፆታ ጨምር |Type new Gender: *M | F | both*',{
        parse_mode:'Markdown'
    })
})
editGenderScene.on('message', async (ctx) => {
    let sex = ctx.message.text.toString().toLowerCase()
    if (sex === 'm' || sex === 'f' || sex === 'both') {
        ctx.session.editData.gender = sex.toUpperCase()
    } else {
       await ctx.reply('wrong input 😕 , type *M | F | both*',{
            parse_mode:'Markdown'
        })
    }
})
editGenderScene.leave()
