import Scene from 'telegraf/scenes/base'
import { initializeSession } from '../../utils'

export const editCelebrityScene = new Scene('edit-celebrity')
editCelebrityScene.enter(async (ctx) => {
    initializeSession(ctx)
    await ctx.reply('ታዋቂ ሰዎችን ጨምር |Add celebrities, _comma separated_',{
        parse_mode:'Markdown'
    })
})
editCelebrityScene.on('message', ctx => {
    ctx.session.editData.celebrity = ctx.message.text.toString()
})
editCelebrityScene.leave()
