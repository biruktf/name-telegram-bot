import Scene from 'telegraf/scenes/base'
import { initializeSession } from '../../utils'

export const editShortFormScene = new Scene('edit-short-form') // pass the name of the scene here
editShortFormScene.enter(async (ctx) => {
    initializeSession(ctx)
   await ctx.reply('ስሙ ሲቆላመጥ|Add short forms(Nickname), _comma separated_')
})
editShortFormScene.on('message', ctx => {
    ctx.session.editData.shortForm = ctx.message.text.toString()
})
editShortFormScene.leave()
