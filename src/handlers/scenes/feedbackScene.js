import Scene from 'telegraf/scenes/base'
import { initializeSession } from '../utils'
import { Bot } from '../../index'
import { config } from '../../config/config'
export const feedbackScene = new Scene('feedback') // pass the name of the scene here
feedbackScene.enter(
    async ctx =>
        await ctx.reply(
            'ያለዎትን አስተያየት ወይም ጥያቄ ይፃፉልን  | write your feedbacks or questions here:',
            {
                parse_mode: 'Markdown',
            }
        )
)
feedbackScene.on('message', async ctx => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        if (ctx.message.text.toString().toLowerCase() === ' ') {
            await ctx.reply('Invalid input please try again! 😕')
        } else {
            await Bot.telegram.sendMessage(
                config.admin_chat_id,
                ` ${ctx.message.text} ... from @${ctx.session.data.createdBy}`,
                {
                    parse_mode: 'Markdown',
                }
            )
            await ctx.reply(
                'ስለአስተያየቶ በጣም እናመሰግናለን |Feedback well received! thank You!😊 '
            )
            ctx.scene.leave()
        }
    } else {
        await ctx.reply('Invalid input please start over! 😕 /start')
        ctx.scene.leave()
    }
})

feedbackScene.leave()
