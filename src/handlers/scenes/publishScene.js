import Scene from 'telegraf/scenes/base'
import { Markup } from 'telegraf'
import { displayFull, initializeSession } from '../utils'

export const publishScene = new Scene('publish')
publishScene.enter(async (ctx) => {
    initializeSession(ctx)
    await ctx.reply(
        displayFull(ctx.session.data),
        {
            parse_mode:'Markdown',
            reply_markup: JSON.stringify({
                'inline_keyboard':[[
                    Markup.callbackButton('Publish', 'PUBLISH'),
                    Markup.callbackButton('Cancel', 'CANCEL'),
                ]]
            })
        }
    )

    ctx.scene.leave()
})

publishScene.leave()
