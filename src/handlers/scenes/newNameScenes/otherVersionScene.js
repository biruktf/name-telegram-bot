import Scene from 'telegraf/scenes/base'
import { Data } from '../../commands/newNameHandler'
import { initializeSession } from '../../utils'

export const otherVersionScene = new Scene('other-version') // pass the name of the scene here
otherVersionScene.enter(async (ctx) =>
    await ctx.reply('ተጨማሪ የስሙ አፃፃፍ |Other versions(English|Misspelled),_comma separated_',{
        parse_mode:'Markdown'
    })
)
otherVersionScene.on('message', async (ctx) => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        if (ctx.message.text.toString().toLowerCase() === 'none') {
            ctx.session.data.otherVersions = ''
        } else {
            ctx.session.data.otherVersions = ctx.message.text.toString().replace(/\s+/g,"");
        }
        ctx.scene.enter('celebrity')
    } else {
        await ctx.reply('Invalid input please start over! /start')
        ctx.scene.leave()
    }
})
otherVersionScene.leave()
