import Scene from 'telegraf/scenes/base'
import { displayFull, initializeSession } from '../../utils'
import { NameModel } from '../../../models/name-model'

export const nameScene = new Scene('name')
nameScene.enter(async (ctx) => {
   await ctx.reply('ስም|Name: ')
})
nameScene.on('message', async (ctx) => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        ctx.session.data.name = ctx.message.text.toString().toLowerCase();
        ctx.scene.enter('gender')

     /*  NameModel
            .findOne({name:ctx.message.text.toString().toLowerCase() })
            .then(data => {
                console.log("siyanad !!! inside then")
              if(data == null){
                  console.log("switching scene")
                  ctx.session.data.name = ctx.message.text.toString().toLowerCase();
                  ctx.scene.enter('gender')
              } else {
                  // ctx.reply(displayFull(data))
                  ctx.reply('Your Name is already posted, please check the channel (@namearchive)   /start')
              }
            })
            .catch(e=>{
                console.log('error encountered while checking if your name is already posted on the channel')
            })*/
    } else {
       await ctx.reply('Invalid input please start over!😕  /start')
        ctx.scene.leave()
    }
})
nameScene.leave()
