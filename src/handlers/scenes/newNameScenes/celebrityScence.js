import Scene from 'telegraf/scenes/base'
import { Data } from '../../commands/newNameHandler'
import { initializeSession } from '../../utils'

export const celebrityScene = new Scene('celebrity') // pass the name of the scene here
celebrityScene.enter(async (ctx) =>
    await ctx.reply('ዝነኛ ሰዎች በዚህ ስም |Celebrities by this name:',{
        parse_mode:'Markdown'
    })
)
celebrityScene.on('message', async (ctx) => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        if (ctx.message.text.toString().toLowerCase() === 'none') {
            ctx.session.data.celebrity = ''
        } else {
            ctx.session.data.celebrity = ctx.message.text.toString()
        }
        ctx.scene.enter('publish')
    } else {
      await ctx.reply('Invalid input please start over! 😕 /start')
        ctx.scene.leave()
    }
})


celebrityScene.leave()
