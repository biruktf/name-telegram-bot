import Scene from 'telegraf/scenes/base'
import { Data } from '../../commands/newNameHandler'
import { initializeSession } from '../../utils'

export const originScene = new Scene('origin') // pass the name of the scene here
originScene.enter(async (ctx) => await ctx.reply('ምንጭ| Origin:'))
originScene.on('message', async (ctx) => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        if (ctx.message.text.toString().toLowerCase() === 'none') {
            ctx.session.data.origin = ''
        } else {
            ctx.session.data.origin = ctx.message.text.toString()
        }
        ctx.scene.enter('description')
    } else {
       await ctx.reply('Invalid input please start over!😕  /start')
        ctx.scene.leave()
    }
})
originScene.leave()
