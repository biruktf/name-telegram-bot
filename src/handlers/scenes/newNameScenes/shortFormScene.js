import Scene from 'telegraf/scenes/base'
import { Data } from '../../commands/newNameHandler'
import { initializeSession } from '../../utils'

export const shortFormScene = new Scene('short-form') // pass the name of the scene here
shortFormScene.enter(async (ctx) =>
   await ctx.reply('ስሙ ሲቆላመጥ|Short form(_Nickname_), _comma separated_',{
        parse_mode:'Markdown'
    })
)
shortFormScene.on('message', async (ctx) => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        if (ctx.message.text.toString().toLowerCase() === 'none') {
            ctx.session.data.shortForm = ''
        } else {
            ctx.session.data.shortForm = ctx.message.text.toString().replace(/\s+/g,"");
        }
        ctx.scene.enter('other-version')
    } else {
       await ctx.reply('Invalid input please start over!😕  /start')
        ctx.scene.leave()
    }
})
shortFormScene.leave()
