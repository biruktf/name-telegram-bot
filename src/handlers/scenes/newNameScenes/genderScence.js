import Scene from 'telegraf/scenes/base'
import { Markup } from 'telegraf'

export const genderScene = new Scene('gender')
genderScene.enter(async (ctx) => {
   await ctx.reply(
        'ፆታ|Gender:',
        Markup.inlineKeyboard([
            Markup.callbackButton('M', 'MALE'),
            Markup.callbackButton('F', 'FEMALE'),
            Markup.callbackButton('both', 'BOTH'),
        ]).extra()
    )
})
genderScene.on('message', async (ctx) => {
    await ctx.reply('please use the buttons!')
})

genderScene.action('MALE', async (ctx) => {
    ctx.session.data.gender = 'M'
   await ctx.answerCbQuery('Male selected')
    ctx.scene.enter('origin').catch(async (e) => {
        console.log(e)
       await ctx.reply('something went wrong try again! 😕 ')
    })
})
genderScene.action('FEMALE', async (ctx) => {
   await ctx.answerCbQuery('Female selected')
    ctx.session.data.gender = 'F'
    ctx.scene.enter('origin').catch(async (e) => {
       await ctx.reply('something went wrong try again! 😕 ')
    })
})
genderScene.action('BOTH', async (ctx) => {
   await ctx.answerCbQuery('Both selected')
    ctx.session.data.gender = 'BOTH'
    ctx.scene.enter('origin').catch(async (e) => {
       await ctx.reply('something went wrong try again! 😕 ')
    })
})
genderScene.leave()
