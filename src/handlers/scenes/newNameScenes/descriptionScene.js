import Scene from 'telegraf/scenes/base'
import { Data } from '../../commands/newNameHandler'
import { initializeSession } from '../../utils'

export const descriptionScene = new Scene('description') // pass the name of the scene here
descriptionScene.enter(async (ctx) => await ctx.reply('ትርጉም |Description:'))
descriptionScene.on('message', async (ctx) => {
    initializeSession(ctx)
    if (
        ctx.message.text.toString() !== '/editname' &&
        ctx.message.text.toString() !== '/post'
    ) {
        if (ctx.message.text.toString().toLowerCase() === 'none') {
            ctx.session.data.description = ''
        } else {
            ctx.session.data.description = ctx.message.text.toString()
        }
        ctx.scene.enter('short-form')
    } else {
      await ctx.reply('invalid input please start over! 😕 /start')
        ctx.scene.leave()
    }
})
descriptionScene.leave()
