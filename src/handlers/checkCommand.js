import { cancelHandler } from './commands/cancelHandler'
import { helpHandler } from './commands/helpHandler'
import { exampleHandler } from './commands/exampleHandler'
import { newNameHandler } from './commands/newNameHandler'
import { listHandler } from './commands/listHandler'
import { startHandler } from './commands/startHandler'
import {
    editNameHandler,
    editNameNameHandler,
} from './commands/editNameHandler'

export const isCommand = Bot => {
    Bot.hears('/cancel', cancelHandler)
    Bot.hears('/help', helpHandler)
    Bot.hears('/example', exampleHandler)
    Bot.hears('/start', startHandler),
    Bot.hears('/mynames', listHandler)
}
