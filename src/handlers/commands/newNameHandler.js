import { initializeSession } from '../utils'

export const newNameHandler = ctx => {
    initializeSession(ctx)
    ctx.session.data.createdBy = ctx.message.from.username
    ctx.scene.enter('name')
}
