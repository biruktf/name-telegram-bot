import { displayFull } from '../utils'

const example1 = {
    name: 'ቡሩክ',
    origin: 'Geez,Amharic',
    gender: 'M',
    description: 'የተባረከ ፡ የተመሰገነ ፡ የተመረቀ ፡ የተቀደሰ ፤ ክቡር ፡ ምስጉን ፡ ምሩቅ ፡ ቅዱስ,የተባረከ በሥራው በአካሔዱ ነውር ነቀፋ የሌለበት ቡሩክ',
    otherVersions: 'Brook,Biruk,Bruk,Bruck',
    shortForm: 'Bura,Bk,Bruke',
    celebrity: 'None',
}
const example2 = {
    name: 'አበበ',
    origin: 'Amharic',
    gender: 'M',
    description: 'በለጸገ, ተስፋፋ, ጥሩ ሆኖ በቀለ, ፈካ',
    otherVersions: 'Abebe',
    shortForm: 'Abe,አቤ',
    celebrity: 'አበበ አረጋይ,አበበ ቢቂላ',
}

export const exampleHandler = ctx => {
    ctx.reply('*example*',{
        parse_mode:'Markdown'
    })
    ctx.reply(displayFull(example1),{
        parse_mode:'Markdown'
    })
    ctx.reply(displayFull(example2),{
        parse_mode:'Markdown'
    })
}
