import { NameModel } from '../../models/name-model'
import { displayFull } from '../utils'
export const listHandler = async ctx => {
    const names = await NameModel.find({ createdBy: ctx.message.from.username })
    if (names.length !== 0) {
        for (let data of names) {
            await ctx.reply(displayFull(data), {
                parse_mode: 'Markdown',
            })
        }
    } else {
        await ctx.reply('Cant find *names*! to add name use /post', {
            parse_mode: 'Markdown',
        })
    }
}
