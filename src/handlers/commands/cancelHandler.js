import {  initializeSession,resetSession } from '../utils'
import {leave } from '../../index'
export const cancelHandler = async (ctx) => {
    initializeSession(ctx)
    resetSession(ctx)
    await ctx.reply('start over!! 😕 /start')
    leave();
}


