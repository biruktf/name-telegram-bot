import { initializeSession } from '../utils'

export const feedbackHandler = ctx => {
    initializeSession(ctx)
    ctx.session.data.createdBy = ctx.message.from.username
    ctx.scene.enter('feedback')
}
