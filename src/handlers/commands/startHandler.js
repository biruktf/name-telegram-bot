import { Markup } from 'telegraf'
import { initializeSession } from '../utils'
export const startHandler = ctx => {
    initializeSession(ctx)

    ctx.session.data.counter = ctx.session.data.counter || 0
    ctx.session.data.counter++
    ctx.session.data.user = ctx.session.data.user || ctx.message.from.username
    console.log(`Message counter:${ctx.session.data.counter}`)
    console.log(`Message User:${ctx.session.data.user}`)

    ctx.session.data.user_chat_id = ctx.chat.id
    ctx.reply(
        'You can interact with me using the following commands:\n' +
            '/help:    *This will Guid you how to use the bot*\n' +
            '/start:   To see this information again\n' +
            '/post:    To post new name to the channel\n' +
            '/editname:  To add more info for existing name\n' +
            '/myNames: To see previously posted names by you\n' +
            '/cancel:  To cancel the request at anytime\n' +
            '/feedback: for your feedbacks and questions' +
            '/example: Examples',
        {
            parse_mode: 'Markdown',
            reply_markup: JSON.stringify({
                keyboard: [
                    [
                        Markup.callbackButton('/start', 'START'),
                        Markup.callbackButton('/help', 'HELP'),
                        Markup.callbackButton('/feedback', 'Feedback'),
                    ],
                    [
                        Markup.callbackButton('/post', 'POST'),
                        Markup.callbackButton('/editname', 'EDIT_NAME'),
                    ],
                    [
                        Markup.callbackButton('/mynames', 'MY_NAMES'),
                        Markup.callbackButton('/cancel', 'CANCEL'),
                        Markup.callbackButton('/example', 'EXAMPLE'),
                    ],
                ],
            }),
        }
    )
}
