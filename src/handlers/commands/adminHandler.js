import { config } from '../../config/config'
import { Markup } from 'telegraf'
import { Bot } from '../../index'
export const adminHandler = async ctx => {
    if (ctx.chat.id == config.admin_chat_id) {
        await Bot.telegram.sendMessage(
            config.admin_chat_id,
            '*Check for updates*',
            {
                parse_mode: 'Markdown',
                reply_markup: JSON.stringify({
                    inline_keyboard: [
                        [
                            Markup.callbackButton(
                                'New Names',
                                'GET_APPLICANTS'
                            ),
                            Markup.callbackButton('Edited Names', 'GET_EDITED'),
                        ],
                    ],
                }),
            }
        )
    }
}
