import { initializeSession } from '../utils'

export const editNameHandler = (ctx) => {
    initializeSession(ctx)
    ctx.session.editData.createdBy = ctx.message.from.username
    ctx.session.editData.editor_user_chat_id = ctx.chat.id
    ctx.scene.enter('firstScene')
}
