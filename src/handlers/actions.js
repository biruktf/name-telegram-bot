import { NameModel } from '../models/name-model'
import { cancelHandler } from './commands/cancelHandler'
import { Markup } from 'telegraf'
import { config } from '../config/config'
import { displayFull, editedVersion, initializeSession } from './utils'
import { NameEditModel } from '../models/name-edit-model'

export const actionHandler = Bot => {
    //events
    Bot.action('GET_APPLICANTS', async (ctx) => {
        await NameModel.find({ approved: false })
            .then(async (data) => {
                if (data.length !== 0) {
                    for (let name of data) {
                        await Bot.telegram.sendMessage(
                            config.admin_chat_id,
                            displayFull(name),
                            {
                                parse_mode: 'Markdown',
                                reply_markup: JSON.stringify({
                                    'inline_keyboard': [[
                                        Markup.callbackButton('Approve', `${name._id}`),
                                        Markup.callbackButton(
                                            'Discard',
                                            `${name._id}_DISCARDED`,
                                        ),
                                    ]],
                                }),
                            },
                        )

                        await Bot.action(`${name._id}`, async (ctx) => {
                            await NameModel.findOneAndUpdate({ _id: name._id }, { approved: true })
                                .then(async (data) => {
                                    await ctx.answerCbQuery(
                                        'Approved successfully 😉',
                                        true,
                                    )

                                    await Bot.telegram.sendMessage(
                                        config.channel_chat_username,
                                        displayFull(data), {
                                            parse_mode: 'Markdown',
                                        },
                                    )
                                }).catch(async e => {
                                    await Bot.telegram.sendMessage(
                                        config.admin_chat_id,
                                        `Error while updating changes 😕`,
                                    )
                                })
                        })

                        await Bot.action(`${name._id}_DISCARDED`, async (ctx) => {
                            await NameModel.findOneAndDelete({ _id: name._id })
                                .then(async (data) => {
                                    await ctx.answerCbQuery(
                                        'Name discarded successfully!\n',
                                    )
                                })
                                .catch(async e => {
                                    await ctx.answerCbQuery(
                                        'Error while discarding data from database ',
                                    )
                                })
                        })
                    }
                } else {
                    await Bot.telegram.sendMessage(
                        config.admin_chat_id,
                        `All names are approved 😉`,
                    )
                }
            }).catch(async e => {
                console.log(e)
                await Bot.telegram.sendMessage(
                    config.admin_chat_id,
                    `No Applicants!! || Error while Getting Applicants 😕`,
                )
            })
    })

    Bot.action('GET_EDITED', async (ctx) => {
        await NameEditModel.find({})
            .then(async (data) => {
                if(data.length !== 0) {
                    for (let name of data) {
                        await Bot.telegram.sendMessage(
                            config.admin_chat_id,
                            `*Edited Name*\n${displayFull(name)}`,
                            {
                                parse_mode: 'Markdown',
                                reply_markup: JSON.stringify({
                                    'inline_keyboard': [
                                        [
                                            Markup.callbackButton(
                                                'Confirm Update',
                                                `${name._id}_EDIT_CONFIRMED`,
                                            ),
                                            Markup.callbackButton(
                                                'Discard update',
                                                `${name._id}_EDIT_DISCARDED`,
                                            ),
                                        ],
                                    ],
                                }),

                            },
                        )
                        await Bot.action(`${name._id}_EDIT_CONFIRMED`, async (ctx) => {
                            const doc = {
                                name: name.name,
                                origin: name.origin,
                                gender: name.gender,
                                description: name.description,
                                celebrity: name.celebrity,
                                shortForm: name.shortForm,
                                otherVersions: name.otherVersions,
                                createdBy: name.createdBy,
                                approved: true,
                            }
                            const editedName = await new NameModel(doc)
                            await editedName.save().then(async (data) => {
                                //delete previous version from Name Model
                                await NameModel.findOneAndDelete({ _id: name.previous_name_id })
                                    .then(async (data) => {
                                        //remove the name from the edit model once its added to name model
                                        await NameEditModel.findOneAndDelete({ _id: name.id })
                                            .then(async (data) => {
                                                await ctx.answerCbQuery(
                                                    'Name Confirmed  successfully 😉',
                                                    true,
                                                )
                                                await Bot.telegram.sendMessage(
                                                    config.channel_chat_username,
                                                    displayFull(data), {
                                                        parse_mode: 'Markdown',
                                                    },
                                                )
                                            })
                                            .catch(async e => {
                                                console.log(e)
                                                await Bot.telegram.sendMessage(
                                                    config.admin_chat_id,
                                                    `Cant Remove data from Edit Db!! 😕 `,
                                                )
                                            })
                                    })
                                    .catch(async (e) => {
                                        console.log(e)
                                        await Bot.telegram.sendMessage(
                                            config.admin_chat_id,
                                            `Cant Remove previous data from  edit Db!! 😕 `,
                                        )
                                    })
                            }).catch((e) => {
                                console.log(e)
                                console.log('Cannot save edited name to NameModel !!')
                            })
                        })

                        await Bot.action(`${name._id}_EDIT_DISCARDED`, async (ctx) => {
                            await NameEditModel.findOneAndDelete({ _id: name._id })
                                .then(async (data) => {
                                    await ctx.answerCbQuery(
                                        'Edited name discarded successfully!\n',
                                    )
                                })
                                .catch(async e => {
                                    console.log(e)
                                    await ctx.answerCbQuery(
                                        'Cant Remove data from Edit Db!!',
                                    )
                                })
                        })

                    }
                }else {
                    await Bot.telegram.sendMessage(
                        config.admin_chat_id,
                        `All Edited names are Approved! 😉`,
                    )
                }
            })
            .catch(async e => {
                await Bot.telegram.sendMessage(
                    config.admin_chat_id,
                    'Cant get names!! All Edited names are Approved! 😉',
                )
            })
    })
    Bot.action('PUBLISH', async (ctx) => {
        const name = await new NameModel(ctx.session.data)
        await name.save()
            .then(async (data) => {
                await ctx.answerCbQuery(
                    'Your name is Saved 👍, Wait for approval\nthank you!',
                    true,
                )
                await Bot.telegram.sendMessage(
                    config.admin_chat_id,
                    'New Name arrived /BBBBBT6',
                )
                initializeSession(ctx)
            })
            .catch(async e => {
                console.log(e)
                await ctx.answerCbQuery('Something went wrong while saving!!', true)
                await cancelHandler(ctx)
            })
    })
    Bot.action('CANCEL', async (ctx) => {
        initializeSession(ctx)
        await ctx.answerCbQuery('canceled 😕  feel free to try again', true)
        await cancelHandler(ctx)
    })

    //=========EDIT NAME ACTIONS============
    Bot.action('SAVE_CHANGES', async (ctx) => {
        await editedVersion(ctx)
        await ctx.reply(
            displayFull(ctx.session.editData),
            {
                parse_mode: 'Markdown',
                reply_markup: JSON.stringify({
                    'inline_keyboard': [[
                        Markup.callbackButton('Publish', 'PUBLISH_EDIT'),
                        Markup.callbackButton('Cancel', 'CANCEL'),
                    ]],
                }),
            },
        )
    })

    Bot.action('PUBLISH_EDIT', async (ctx) => {
        const name = await new NameEditModel(ctx.session.editData)
        await name.save()
            .then(async (data) => {
                await ctx.answerCbQuery('Name modified 👍, Wait for approval\n thank you! ', true)
                await Bot.telegram.sendMessage(
                    config.admin_chat_id,
                    `Name Edited ${data.name} use  /BBBBBT6`,
                )
                initializeSession(ctx)
            })
            .catch(async e => {
                console.log(e)
                await ctx.answerCbQuery('something went wrong while saving!!', true)
                // cancelHandler(ctx)
            })
    })


  

}
