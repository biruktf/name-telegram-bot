const mongoose = require('mongoose')
const { Schema } = mongoose

const nameSchema = new Schema({
    createdOn: { type: Date, required: true, default: Date.now },
    createdBy: { type: String},
    origin: { type: String },
    shortForm: { type: String },
    otherVersions: { type: String },
    name: { type: String },
    description: { type: String },
    popularPeople: { type: String },
    gender: { type: String },
    celebrity: { type: String },
    approved: { type: Boolean, default: false },
})

export const NameModel = mongoose.model('Name', nameSchema)
